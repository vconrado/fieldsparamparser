
import com.vconrado.fieldsparamparser.FieldParam;
import com.vconrado.fieldsparamparser.FieldsParamParser;
import com.vconrado.fieldsparamparser.exception.FieldParamParserException;
import java.util.List;

/**
 *
 * @author Vitor
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String fieldsParam = "a,b(fields=b1(fields=b1-1,b1-2),b2),c,d(fields=d1,d2,d3(fields=d3-1,d3-2))";
        //String fieldsParam = "a";
        //String fieldsParam = "a,a(fields=x,h),b,c";
        //String fieldsParam = "a,b(fields=c)";

        List<FieldParam> fieldParams;
        try {
            fieldParams = FieldsParamParser.parseFieldParam(fieldsParam);
            for (FieldParam fp : fieldParams) {
                System.out.println(fp);
            }
        } catch (FieldParamParserException ex) {
            ex.printStackTrace();
        }
    }
}
