package com.vconrado.fieldsparamparser;

import com.vconrado.fieldsparamparser.exception.FieldParamParserException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class FieldsParamParser {

    public static List<FieldParam> parseFieldParam(String fieldsStr) throws FieldParamParserException {
        return FieldsParamParser._parseFieldParam("(fields=" + fieldsStr + ")");
    }

    private static List<FieldParam> _parseFieldParam(String fieldsStr) throws FieldParamParserException {

        if (fieldsStr.length() < 10) {
            throw new FieldParamParserException("String too small. Expected: (fields=......)");
        }
        if (!fieldsStr.startsWith("(fields=") || !fieldsStr.endsWith(")")) {
            throw new FieldParamParserException("Invalid fields format. Expected: (fields=......)");
        }

        fieldsStr = fieldsStr.substring(8, fieldsStr.length() - 1);

        List<FieldParam> fieldParams = new ArrayList<>();
        String field = "";
        String subField = "";
        Stack<Character> parenthensiesStack = new Stack<>();

        for (int i = 0; i < fieldsStr.length(); i++) {
            char c = fieldsStr.charAt(i);
            switch (c) {
                case '(':
                    parenthensiesStack.push(c);
                    subField += c;
                    break;
                case ')':
                    if (parenthensiesStack.empty()) {
                        throw new FieldParamParserException("Simbol '(' not expected : '" + fieldsStr.substring(i) + "'.");
                    }
                    parenthensiesStack.pop();
                    subField += c;
                    break;
                case ',':
                    if (parenthensiesStack.empty()) {
                        if (field.length() == 0) {
                            throw new FieldParamParserException("Empty field is not valid: '" + fieldsStr.substring(i) + "'.");
                        }
                        FieldParam fp = new FieldParam();
                        fp.setField(field);
                        if (subField.length() > 0) {
                            fp.setFields(FieldsParamParser._parseFieldParam(subField));
                        }
                        fieldParams.add(fp);
                        field = "";
                        subField = "";
                    } else {
                        subField += c;
                    }
                    break;
                default:
                    if (parenthensiesStack.isEmpty()) {
                        field += c;
                    } else {
                        subField += c;
                    }
            }
        }
        if (!parenthensiesStack.empty()) {
            throw new FieldParamParserException("Unexpected end of fields specification.");
        }
        FieldParam fp = new FieldParam();
        fp.setField(field);
        if (subField.length() > 0) {
            fp.setFields(FieldsParamParser._parseFieldParam(subField));
        }
        fieldParams.add(fp);
        return fieldParams;
    }
}
