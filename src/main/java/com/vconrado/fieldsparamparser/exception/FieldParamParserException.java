package com.vconrado.fieldsparamparser.exception;

public class FieldParamParserException extends Exception {

    public FieldParamParserException() {

    }

    public FieldParamParserException(String msg) {
        super(msg);
    }
}
