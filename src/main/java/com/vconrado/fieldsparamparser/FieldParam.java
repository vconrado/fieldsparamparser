package com.vconrado.fieldsparamparser;

import java.util.ArrayList;
import java.util.List;

public class FieldParam {

    String field;
    List<FieldParam> fields = new ArrayList();

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public List<FieldParam> getFields() {
        return fields;
    }

    public void setFields(List<FieldParam> fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Field=")
                .append(field)
                .append("\n");

        if (fields.size() > 0) {
            sb.append("Subfields of ")
                    .append(field)
                    .append("\n");

            for (FieldParam f : fields) {
                sb.append(f.toString());
            }
            sb.append("End subfields of ")
                    .append(field)
                    .append("\n");

        }
        return sb.toString();
    }

}
